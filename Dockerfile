FROM cloudron/base:0.10.0

ENV ITOPVERSION=2.4.1

RUN mkdir -p /app/code/inst /run/itop/sessions /app/data
WORKDIR /app/code

# get itop and extract it
RUN wget https://downloads.sourceforge.net/project/itop/itop/${ITOPVERSION}/iTop-2.4.1-3714.zip && \
    unzip iTop-2.4.1-3714.zip && find /app/code -type f -maxdepth 1 -delete && mv /app/code/web/* /app/code/ && \
    mv /app/code/log /app/code/log_orig && \
    mv /app/code/data /app/code/data_orig && \
    ln -s /app/data/log /app/code/log && \
    ln -s /app/data/data /app/code/data && \
    ln -s /app/data/conf /app/code/conf && \
    ln -s /app/data/env-production /app/code/env-production && \
    ln -s /app/data/toolkit /app/code/toolkit && \
    ln -s /app/data/env-production-build /app/code/env-production-build

# install graphviz
RUN apt-get update && apt-get install -y graphviz php7.0-soap && rm -rf /var/cache/apt /var/lib/apt/lists

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/itop.conf /etc/apache2/sites-enabled/itop.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure mod_php
RUN a2enmod php7.0
RUN crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP upload_max_size 64M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP post_max_size 128M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/7.0/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.save_path /run/itop/sessions && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.0/apache2/php.ini Session session.gc_divisor 100

ADD unattended_install.php default-params.xml /app/code/inst/
ADD start.sh /app/

RUN chown -R www-data.www-data /app/code /run/itop /app/data

CMD [ "/app/start.sh" ]
