#!/bin/bash

set -eux

if [[ ! -f /app/data/env-production/itop-config/config.php ]]; then
    echo "=> Detected first run"

    # set up default directories with write access and copy the data from readonly
    mkdir -p /app/data/log /app/data/env-production /app/data/env-production-build /app/data/conf/ /app/data/data /app/data/toolkit
    cp -R /app/code/log_orig/* /app/data/log
    cp -R /app/code/inst/* /app/data/toolkit
    cp -R /app/code/data_orig/* /app/data/data

    # update mysql data in xml for installation
    sed -i "s/APP_DOMAIN/${APP_DOMAIN}/" /app/data/toolkit/default-params.xml
    sed -i "s/MYSQL_HOST/${MYSQL_HOST}/" /app/data/toolkit/default-params.xml
    sed -i "s/MYSQL_DATABASE/${MYSQL_DATABASE}/" /app/data/toolkit/default-params.xml
    sed -i "s/MYSQL_USERNAME/${MYSQL_USERNAME}/" /app/data/toolkit/default-params.xml
    sed -i "s/MYSQL_PASSWORD/${MYSQL_PASSWORD}/" /app/data/toolkit/default-params.xml

    # install
    cd /app/code/toolkit
    php unattended_install.php default-params.xml

    # update domain with trailing slash
    sed -i "s,'app_root_url'.*,'app_root_url' => '${APP_ORIGIN}\/'\,," /app/data/conf/production/config-itop.php

    # update mysql data
    sed -i "s/'db_host'.*/'db_host' => getenv(\"MYSQL_HOST\"),/" /app/data/conf/production/config-itop.php
    sed -i "s/'db_name'.*/'db_name' => getenv(\"MYSQL_DATABASE\"),/" /app/data/conf/production/config-itop.php
    sed -i "s/'db_user'.*/'db_user' => getenv(\"MYSQL_USERNAME\"),/" /app/data/conf/production/config-itop.php
    sed -i "s/'db_pwd'.*/'db_pwd' => getenv(\"MYSQL_PASSWORD\"),/" /app/data/conf/production/config-itop.php

    # update ldap data
    sed -i "s/'localhost'/getenv(\"LDAP_SERVER\")/" /app/data/conf/production/config-itop.php
    sed -i 's/389/getenv("LDAP_PORT")/' /app/data/conf/production/config-itop.php
    sed -i "s/'dc=yourcompany,dc=com'/getenv(\"LDAP_USERS_BASE_DN\")/" /app/data/conf/production/config-itop.php
    sed -i "s/'user_query'.*/'user_query' => '(\&(username=\%1\$s))',/" /app/data/conf/production/config-itop.php

fi

# set absolute paths, else the editor and user login won't work
sed -i "s/..\/..\/approot.inc.php/\/app\/code\/approot.inc.php/" /app/data/env-production/itop-config/config.php
sed -i "s/__DIR__ \. '\/..\/..\/approot.inc.php'/'\/app\/code\/approot.inc.php'/" /app/data/env-production/itop-portal-base/index.php

echo "=> Ensuring runtime directories"
chown -R www-data.www-data /app/data /run

echo "=> Run iTop"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
