This app packages iTop 2.4.1

iTop is the hub to build a single solution that covers various customers while protecting the confidentiality essential to every organization.

Designed by experienced IT service professionals, iTop has been created to manage the complexity of shared infrastructures. iTop gives you the ability to analyze the impact of an incident or a change on the various services and contracts that you have to fulfil.
